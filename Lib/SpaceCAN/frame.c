#include "frame.h"

/**
 * @brief Creates and initializes new can frame.
 * @param frame Pointer to frame
 * @param can_id CAN ID
 * @param data Pointer to data.
 * @param length Number of data bytes. Maximum 8 bytes.
 */
int spacecan_frame_init(Frame_t *frame, uint16_t can_id, uint8_t *data, uint8_t length)
{
	frame->can_id = can_id;

	if (length <=8) {
		frame->length = length;
	} else {
		frame->length = 8;
	}

	for (uint8_t i = 0; i < length; ++i) {
		frame->data[i] = data[i];
	}
	return 0;
}
