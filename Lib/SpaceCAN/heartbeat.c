#include "heartbeat.h"

/**
 * @brief Creates and initializes a new heartbeat producer.
 * @param network Pointer to a Network_t structure
 */
int spacecan_heartbeat_producer_init(HeartbeatProducer_t *heartbeat_producer, Network_t *network)
{

	heartbeat_producer->network = network;
	heartbeat_producer->running = pdFALSE;
	heartbeat_producer->period = pdMS_TO_TICKS(1000);
	heartbeat_producer->taskHandle = NULL;

	return 0;
}

/**
 * @brief Starts transmission of heartbeats with the given HeartbeatProducer.
 * @param heartbeat_producer Pointer to a HeartbeatProducer_t which should be used to transmit heartbeats.
 * @param period Integer which defines the length of the transmission period
 */
void spacecan_heartbeat_producer_start(HeartbeatProducer_t *heartbeat_producer, BaseType_t period_in_ms)
{
	HAL_UART_Transmit(&huart2, (uint8_t *) "Enter: Start task\r\n", 19, 1000);
	if (heartbeat_producer->running == pdTRUE) {
		spacecan_heartbeat_producer_stop(heartbeat_producer);
		HAL_CAN_Stop(&hcan);
	}
	HAL_UART_Transmit(&huart2, (uint8_t *) "Before: Start task\r\n", 20, 1000);
	//TODO: remove when bus and network implemented
	HAL_CAN_Start(&hcan);

	heartbeat_producer->period = pdMS_TO_TICKS(period_in_ms);

	BaseType_t success = xTaskCreate
						 (
							 (TaskFunction_t)spacecan_heartbeat_producer_task,
							 "Heartbeat Producer",
							 10,
							 (void*)heartbeat_producer,
							 0,
							 &(heartbeat_producer->taskHandle)
						 );
	if (success == pdPASS) {
		HAL_UART_Transmit(&huart2, (uint8_t *) "Task created\r\n", 15, 1000);
	} else {
		HAL_UART_Transmit(&huart2, (uint8_t *) "Task creation failed\r\n", 22, 1000);
	}
	heartbeat_producer->running = pdTRUE;

}

/**
 * @brief Stops transmission of heartbeats on the given HeartbeatProducer.
 * @param heartbeat_producer Pointer to a HeartbeatProducer_t struct which should stop the transmission of heartbeats.
 */
void spacecan_heartbeat_producer_stop(HeartbeatProducer_t *heartbeat_producer)
{
	if (heartbeat_producer->running == pdTRUE) {
		vTaskDelete(&(heartbeat_producer->taskHandle));
		heartbeat_producer->running = pdFALSE;
		HAL_CAN_Stop(&hcan);
	}
}

/**
 * @brief Heartbeat producer task.
 * @param pvParameters Used for parameters.
 */
void spacecan_heartbeat_producer_task(void *pvParameters)
{
	TickType_t xLastWakeTime;

	HeartbeatProducer_t *heartbeat_producer = (HeartbeatProducer_t *) pvParameters;

	xLastWakeTime = xTaskGetTickCount();

	for( ;; ) {

		spacecan_heartbeat_producer_send(heartbeat_producer);
		vTaskDelayUntil(&xLastWakeTime, heartbeat_producer->period);
	}
}

/**
 * @brief Transmits one heartbeat.
 * @param heartbeat_producer Pointer to a HeartbeatProducer_t which should be used to transmit heartbeats.
 */
int spacecan_heartbeat_producer_send(HeartbeatProducer_t *heartbeat_producer)
{
	//network_send(heartbeat_producer->network, heartbeat_producer->frame);
	uint32_t mail_box;
	HAL_CAN_Start(&hcan);
	CAN_TxHeaderTypeDef txHeader = {0x700, 0x0, CAN_ID_STD, CAN_RTR_DATA, 1, DISABLE};
	HAL_StatusTypeDef status;
	status = HAL_CAN_AddTxMessage(&hcan, &txHeader, (uint8_t *) 0, &mail_box);
	if (status == HAL_OK) {
		HAL_UART_Transmit(&huart2, (uint8_t *) "Success\r\n", 9, 1000);
		return 0;
	} else {
		HAL_UART_Transmit(&huart2, (uint8_t *) "Failed\r\n", 8, 1000);
		return -1;
	}
}

int spacecan_heartbeat_consumer_init(HeartbeatConsumer_t *heartbeat_consumer, Network_t *network, uint16_t max_heartbeat_missed, uint16_t max_bus_switches)
{

	heartbeat_consumer->network = network;
	heartbeat_consumer->running = pdFALSE;
	heartbeat_consumer->period = pdMS_TO_TICKS(1000);
	heartbeat_consumer->taskHandle = NULL;
	heartbeat_consumer->max_heartbeat_missed = max_heartbeat_missed;
	heartbeat_consumer->max_bus_switches = max_bus_switches;
	heartbeat_consumer->heartbeat_missed = 0;
	heartbeat_consumer->bus_switches = 0;
	heartbeat_consumer->received_frame = pdFALSE;

	return 0;
}

void spacecan_heartbeat_consumer_start(HeartbeatConsumer_t *heartbeat_consumer, BaseType_t period_in_ms)
{
	if (heartbeat_consumer->running == pdTRUE) {
		spacecan_heartbeat_consumer_stop(heartbeat_consumer);
		//TODO: remove when bus and network implemented
		HAL_CAN_Stop(&hcan);
	}
	//TODO: remove when bus and network implemented
	HAL_CAN_Start(&hcan);
	heartbeat_consumer->period = pdMS_TO_TICKS(period_in_ms);
	xTaskCreate
	(
		(TaskFunction_t)spacecan_heartbeat_consumer_task,
		"Heartbeat Consumer",
		1000,
		(void*)heartbeat_consumer,
		0,
		&(heartbeat_consumer->taskHandle)
	);
	heartbeat_consumer->running = pdTRUE;
}

void spacecan_heartbeat_consumer_task(void *pvParameters)
{

	TickType_t xLastWakeTime;

	HeartbeatConsumer_t *heartbeat_consumer = (HeartbeatConsumer_t *) pvParameters;

	xLastWakeTime = xTaskGetTickCount();

	for( ;; ) {

		if(!heartbeat_consumer->received_frame) {
			heartbeat_consumer->heartbeat_missed++;
			if(heartbeat_consumer->heartbeat_missed >= heartbeat_consumer->max_bus_switches) {
				if(heartbeat_consumer->bus_switches < heartbeat_consumer->max_bus_switches || !heartbeat_consumer->max_bus_switches) {
					//TODO: actually switch the bus...
					heartbeat_consumer->bus_switches++;
					heartbeat_consumer->heartbeat_missed = 0;
				}
			}
		}
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(heartbeat_consumer->period));
	}
}

void spacecan_heartbeat_consumer_stop(HeartbeatConsumer_t *heartbeat_consumer)
{
	if (heartbeat_consumer->running == pdTRUE) {
		vTaskDelete(&(heartbeat_consumer->taskHandle));
		heartbeat_consumer->running = pdFALSE;
		HAL_CAN_Stop(&hcan);
	}
}
