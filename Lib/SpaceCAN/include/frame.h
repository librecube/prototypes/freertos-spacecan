#ifndef FRAME_H_
#define FRAME_H_
#include "stdint.h"
typedef struct Frame {
	uint16_t can_id;
	uint8_t data[8];
	uint8_t length;
} Frame_t;

int spacecan_frame_init(Frame_t *frame, uint16_t can_id, uint8_t *data, uint8_t length);

#endif /* FRAME_H_*/
