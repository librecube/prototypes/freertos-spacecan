#ifndef HEARTBEAT_H_
#define HEARTBEAT_H_
#include "main.h"
#include "frame.h"
#include "network.h"
#include "FreeRTOS.h"
#include "task.h"
#include "can.h"
#include "usart.h"

typedef struct HeartbeatProducer {
	Network_t *network;
	BaseType_t running;
	TickType_t period;
	TaskHandle_t taskHandle;
} HeartbeatProducer_t;

typedef struct HeartbeatConsumer {
	Network_t *network;
	BaseType_t running;
	uint16_t max_heartbeat_missed;
	uint16_t max_bus_switches;
	uint16_t heartbeat_missed;
	uint16_t bus_switches;
	BaseType_t received_frame;
	TaskHandle_t taskHandle;
	TickType_t period;
} HeartbeatConsumer_t;

int spacecan_heartbeat_producer_init(HeartbeatProducer_t *heartbeat_producer, Network_t *network);
void spacecan_heartbeat_producer_start(HeartbeatProducer_t *heartbeat_producer, BaseType_t period_in_ms);
void spacecan_heartbeat_producer_stop(HeartbeatProducer_t *heartbeat_producer);
int spacecan_heartbeat_producer_send(HeartbeatProducer_t *heartbeat_producer);
void spacecan_heartbeat_producer_task(void *pvParameters);

int spacecan_heartbeat_consumer_init(HeartbeatConsumer_t *heartbeat_consumer, Network_t *network, uint16_t max_heartbeat_missed, uint16_t max_bus_switches);
void spacecan_heartbeat_consumer_start(HeartbeatConsumer_t *heartbeat_consumer, BaseType_t period_in_ms);
void spacecan_heartbeat_consumer_stop(HeartbeatConsumer_t *heartbeat_consumer);
void spacecan_heartbeat_consumer_timer(HeartbeatConsumer_t *heartbeat_consumer);
int spacecan_heartbeat_consumer_received(HeartbeatConsumer_t *heartbeat_consumer, Frame_t frame);
void spacecan_heartbeat_consumer_task(void *pvParameters);

#endif /* HEARTBEAT_H_*/
