#ifndef NETWORK_H_
#define NETWORK_H_

#include "bus.h"
#include "frame.h"
#include "FreeRTOS.h"

typedef struct Network {
	uint8_t node_id;
	Bus_t *bus_a;
	Bus_t *bus_b;
	Bus_t *active_bus;
	BaseType_t active;
} Network_t;

Network_t spacecan_network_init(Network_t *network, uint8_t node_id, Bus_t *bus_a, Bus_t *bus_b);
void spacecan_network_set_filters(Network_t *network, unsigned int *filters, int len_filters);
Bus_t* spacecan_network_get_active_bus(Network_t *network);
void spacecan_network_set_active_bus(Network_t *network, Bus_t *bus);
void spacecan_network_switch_bus(Network_t *network);
void spacecan_network_start(Network_t *network);
void spacecan_network_stop(Network_t *network);
BaseType_t spacecan_network_is_active(Network_t *network);
void spacecan_network_shutdown(Network_t *network);
void spacecan_network_send(Network_t *network, Frame_t frame);
void spacecan_network_process(Network_t *network);

#endif /* NETWORK_H_ */
