## SpaceCAN for FreeRTOS in C
Tools used for Development:
* STMCubeMX
* STM32F103RB

## How to build & flash
In the project directory and with stlink installed run:
```
make
st-flash --reset write build/SpaceCAN_C.bin 0x08000000
```
